/*
Copyright Contributors, Pseudospace Virtual Worlds

This file is part of libPseudospace.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;

namespace Pseudospace.Types
{
    /// <summary>
    ///  Defines the initialization property of an IpsComponent
    ///  First is used for low level components such as the configuration registry.
    ///  Middle is used for second level components such as consoles, http servers, databases, etc that rely on the first level
    ///  Last is used for high level components such as those that rely on the first and middle levels.
    /// </summary>
    public enum psComponentInitializationPriority
    {
        First = 1,
        Middle = 2,
        Last = 3,
    }

    public interface IpsComponent
    {
    	/// <summary>
   		/// Name of the component</summary>
        string ComponentName { get; }
        
        /// <summary>
   		/// Initialization priority of the component</summary>
        psComponentInitializationPriority InitPriority { get; }

        bool Initialized { get; }
        bool PostInitialized { get; }

        /// <summary>
   		/// Tasks to be performed during the initialization of the component.</summary>
        void Initialize(IpsComponentRegistry ComponentRegistry);
        /// <summary>
   		/// Tasks to be performed after the initialization of the component.</summary>
        void PostInitialize();
        
        /// <summary>
   		/// Refresh this component's link to its dependant components
		/// (should be run after the contents of the component registry have changed during runtime).</summary>
        void Refresh();
    }
    
    public interface IpsReplacableComponent : IpsComponent, IDisposable {}
}

