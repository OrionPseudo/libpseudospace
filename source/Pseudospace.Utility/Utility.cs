/*
Copyright Contributors, Pseudospace Virtual Worlds

This file is part of libPseudospace.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;

namespace Pseudospace.Utility
{
	public static class psUtility
	{
		private static Random m_Random = new Random(Convert.ToInt32(ConvertTimestamp(DateTime.Now)));

		public static int GetRandom(int RangeStart, int RangeStop)
		{
			return(m_Random.Next(RangeStart, RangeStop));	
		}

        public static double Minutes2Milliseconds(int Minutes)
        {
            return (Convert.ToDouble(Minutes * 60000));
        }

        public static double Minutes2Seconds(int Minutes)
        {
            return (Convert.ToDouble(Minutes * 60));
        }

		public static bool IntToBool(int IntBool)
        {
            if (IntBool == 0)
            {
                return (false);
            }
            else
            {
                return (true);
            }
        }
	
        public static int IntToBool(bool BoolInt)
        {
            if (BoolInt == false)
            {
                return (0);
            }
            else
            {
                return (1);
            }
        }

        public static readonly DateTime ZeroHour = new DateTime(1970, 1, 1, 0, 0, 0, 0);

		public static DateTime ConvertTimestamp(double timestamp)
        {
            //create a new DateTime value based on the Unix Epoch
            DateTime converted = new DateTime(1970, 1, 1, 0, 0, 0, 0);

            //add the timestamp to the value
            DateTime newDateTime = converted.AddSeconds(timestamp);

            //return the value in string format
            return newDateTime;
        }

        public static double ConvertTimestamp(DateTime ToConvert)
        {
            return ((ToConvert.Ticks - 621355968000000000) / 10000000);
        }
	}
}

