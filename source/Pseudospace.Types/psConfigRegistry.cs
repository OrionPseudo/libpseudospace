﻿/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Collections.Concurrent;

namespace Pseudospace.Types
{
    public class psConfigRegistry : IpsConfigRegistry, IpsComponent
    {
        private ConcurrentDictionary<string, ConcurrentDictionary<string, object>> m_Configs = new ConcurrentDictionary<string, ConcurrentDictionary<string, object>>();
        private IpsComponentRegistry m_ComponentRegistry;
        private IpsFeedbackController m_Feedback;
        private bool m_IsInitialized = false;

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
            m_ComponentRegistry = ComponentRegistry;
            m_Feedback = m_ComponentRegistry.GetComponent<IpsFeedbackController>();
            m_IsInitialized = true;
        }
        
        public void Refresh()
        {
        	if(m_IsInitialized) {m_Feedback = m_ComponentRegistry.GetComponent<IpsFeedbackController>();}
        }

        public void PostInitialize()
        {}

        public string ComponentName { get { return("Configuration Registry"); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.First); } }
        public bool Initialized { get { return(m_IsInitialized); } }
        public bool PostInitialized { get { return(true); } }

        public bool AddSection(string SectionName)
        {
            if(!m_Configs.TryAdd(SectionName, new ConcurrentDictionary<string, object>()))
            {
                m_Feedback.DebugMessage(ComponentName, "Unable to add configuration section: " + SectionName + "! Section already exists!");
                return(false);
            }
            return(true);
        }

        public bool DeleteSection(string SectionName)
        {
            ConcurrentDictionary<string, object> Junk;
            return(m_Configs.TryRemove(SectionName, out Junk));
        }

        public bool HasSection(string SectionName)
        {
            return(m_Configs.ContainsKey(SectionName));
        }

        public bool AddOrUpdateSectionItem(string SectionName, string Key, object Item)
        {
            if (m_Configs.ContainsKey(SectionName))
            {
                m_Configs[SectionName].AddOrUpdate(Key, Item, (k, v) => v = Item);
                return(true);
            }
            else
            {
                m_Feedback.DebugMessage(ComponentName, "Add item key " + Key + " from section " + SectionName + " failed.  Section does not exist.");
                return(false);
            }
        }

        public bool DeleteSectionItem(string SectionName, string Key)
        {
            if (m_Configs.ContainsKey(SectionName))
            {
                object Junk;
                return(m_Configs[SectionName].TryRemove(Key, out Junk));
            }
            else
            {
                m_Feedback.DebugMessage(ComponentName, "Delete item key " + Key + " from section " + SectionName + " failed.  Section does not exist.");
                return(false);
            }

        }

        public T GetSectionItem<T>(string SectionName, string Key)
        {
            if (m_Configs.ContainsKey(SectionName))
            {
                object Item;
                
                if(m_Configs[SectionName].TryGetValue(Key, out Item))
                {
                    return((T)Item);
                }
                else
                {
                    m_Feedback.DebugMessage(ComponentName, SectionName + " -> " + Key + " was requested but not found in the registry!");
                    return(default(T)); 
                }
            }
            else
            {
                m_Feedback.DebugMessage(ComponentName, "Get item key " + Key + " from section " + SectionName + " failed.  Section does not exist.");
                return(default(T)); 
            }
        }
    }
}
