/*
Copyright Contributors, Pseudospace Virtual Worlds

This file is part of libPseudospace.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Reflection;

namespace Pseudospace.Types
{
    public class psDLLLoader : IpsDLLLoader
    {
        private IpsFeedbackController m_Feedback;
        private bool m_Initialized = false;

        public string ComponentName { get { return("DLL Loader"); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.First); } }

        public bool Initialized { get { return(m_Initialized); } }
        public bool PostInitialized { get { return(true); } }
        
        private IpsComponentRegistry m_ComponentRegistry;

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
     	    m_ComponentRegistry = ComponentRegistry;
            m_Feedback = m_ComponentRegistry.GetComponent<IpsFeedbackController>();
            m_Initialized = true;
        }

        public void Refresh()
        {
        	if(m_Initialized) {m_Feedback = m_ComponentRegistry.GetComponent<IpsFeedbackController>();}
        }
        
        public void PostInitialize() {}
        
        public void Dispose() {}

        //Plugin loader code borrowed with some modifications from the OpenSimulator project, http://www.opensimulator.org/
        //See license file for more information.
      
        public T LoadDLL<T>(string dllName, string className, Object[] args) where T : class
        {
            string interfaceName = typeof(T).ToString();

            try
            {
                Assembly pluginAssembly = Assembly.LoadFrom(dllName);

                foreach (Type pluginType in pluginAssembly.GetTypes())
                {
                    if (pluginType.IsPublic)
                    {
                        if (className != String.Empty
                            && pluginType.ToString() != pluginType.Namespace + "." + className)
                            continue;

                        Type typeInterface = pluginType.GetInterface(interfaceName, true);

                        if (typeInterface != null)
                        {
                            T plug = null;
                            try
                            {
                                plug = (T)Activator.CreateInstance(pluginType,
                                        args);
                            }
                            catch (Exception e)
                            {
                                if (!(e is System.MissingMethodException))
                                {    //m_log.ErrorFormat("Error loading plugin from {0}, exception {1}", dllName, e.InnerException);
                                    m_Feedback.Exception(ComponentName, "Error loading plugin from " + dllName + "!", e);
                                }
                                return null;
                            }

                            return plug;
                        }
                    }
                }

                return null;
            }
            catch (ReflectionTypeLoadException rtle)
            {
                /*m_log.Error(string.Format("Error loading plugin from {0}:\n{1}", dllName,
                    String.Join("\n", Array.ConvertAll(rtle.LoaderExceptions, e => e.ToString()))),
                    rtle);*/
                m_Feedback.Exception(ComponentName, string.Format("Error loading plugin from " + dllName + ": " +
                    String.Join("\n", Array.ConvertAll(rtle.LoaderExceptions, e => e.ToString()))),
                    rtle);
                return null;
            }
            catch (Exception e)
            {
                //m_log.Error(string.Format("Error loading plugin from {0}", dllName), e);
                m_Feedback.Exception(ComponentName, "Error loading plugin from " + dllName + "!", e);
                return null;
            }
        }
    }
}

