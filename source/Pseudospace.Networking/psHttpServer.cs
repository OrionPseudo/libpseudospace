/*
Copyright Contributors, Pseudospace Virtual Worlds

This file is part of libPseudospace.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Threading;
using System.Net;
using System.Collections.Concurrent;

using Amib.Threading;
using Pseudospace.Types;

namespace Pseudospace.Networking.HTTP
{	
	public class psHttpServer : IpsHttpServer, IpsComponent
	{
		private bool running;
		private string m_BaseURL;
		
		private HttpListener m_Listener;
		private Thread m_ListenerThread;
		
		private SmartThreadPool m_ThreadPool;

		private long m_RequestsProcessed = 0;
		private long m_GetRequests = 0;
		private long m_PostRequests = 0;
		private long m_DeleteRequests = 0;

        private int m_IdleTimeout = 10000;

        private bool m_IsInitialized = false;
        private bool m_IsPostInitialized = false;

		private IpsComponentRegistry m_Registry;
		private IpsFeedbackController m_Feedback;

		ConcurrentDictionary<string, psHttpRequestDelegate> m_Paths = new ConcurrentDictionary<string, psHttpRequestDelegate>();

        public string ComponentName { get { return("HTTP Server"); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.Middle); } }

        public bool Initialized { get { return(m_IsInitialized); } }
        public bool PostInitialized { get { return(m_IsPostInitialized); } }

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
            m_Listener = new HttpListener();
            m_ThreadPool = new SmartThreadPool(IdleTimeout);    
            m_Registry = ComponentRegistry;
            m_Feedback = m_Registry.GetComponent<IpsFeedbackController>();
            m_IsInitialized = true;
        }
        
        public void Refresh()
        {
        	if(m_IsInitialized) { m_Feedback = m_Registry.GetComponent<IpsFeedbackController>(); }
        }

        public void PostInitialize()
        {
            Start();
            m_IsPostInitialized = true;
        }

		public long RequestsProcessed
		{
			get { return (m_RequestsProcessed); }	
		}
		
		public long GetRequests
		{
			get { return (m_GetRequests); }	
		}
		
		public long PostRequests
		{
			get { return (m_PostRequests); }
		}
		
		public long DeleteRequests
		{
			get { return (m_DeleteRequests); }
		}
		
		public long ActiveThreads
		{
			get { return (m_ThreadPool.ActiveThreads); }
		}
		
		public long ThreadsInUse
		{
			get { return (m_ThreadPool.InUseThreads); }
		}
		
        public int IdleTimeout { get { return(m_IdleTimeout); } set { m_IdleTimeout = value; } }

		private void TabulateServerStats(string HttpMethod)
		{
			//Decipher stats for get / post / delete
			
			m_RequestsProcessed ++;

			switch(HttpMethod)
			{
				case "POST":
					m_PostRequests ++;
					break;
			 	case "GET": 
					m_GetRequests ++;
					break;
				case "DELETE":
					m_DeleteRequests ++;
					break;
			}
		}

		public string BaseURL
		{
			get { return(m_BaseURL);}
			
			set
			{
				if(!running)
				{
					m_BaseURL = value;
					m_Listener.Prefixes.Add(value);
				}
				else
				{
					m_Feedback.Exception(ComponentName, "HTTP INIT ERROR", new Exception("Cannot change base URL while HTTP server is running!"));
				}
			}
		}

		public void RegisterPath(string Path, psHttpRequestDelegate D)
		{
			m_Paths.AddOrUpdate(Path, D, (k, v) => v = D);
		}

		public void DeRegisterPath(string Path)
		{
			psHttpRequestDelegate Junk;
			m_Paths.TryRemove(Path, out Junk);
		}

		public void Start ()
		{
			if (!running) 
			{
				running = true;
				m_Listener.Start ();
				m_ListenerThread = new Thread (new ThreadStart (Listen));
				m_ListenerThread.Start ();
			} 
			else 
			{
				m_Feedback.Exception(ComponentName, "HTTP INIT ERROR", new Exception("HTTP server is already running!"));
			}
		}
		
		public void Stop()
		{
            //stop Listen loop and wait for all working threads to finish.
            running = false;
            m_Listener.Stop();
			m_ThreadPool.WaitForIdle();
		}
		
		private void Listen()
		{
			while(running)
			{
				try
				{
			  		m_ThreadPool.QueueWorkItem(new WorkItemCallback(this.ProcessRequest), m_Listener.GetContext());
				}
				catch (HttpListenerException E) 
				{ 
                    if(running) //No need to raise a fuss if we're shutting down!
                    {
    					m_Feedback.Exception(ComponentName, "HTTP Runtime Exception", E);
    					break; 
                    }
				} 
			}
		     
		}	
		
	    private object ProcessRequest (object HttpContext)
		{
			try 
			{
				HttpListenerContext m_Context = (HttpListenerContext)HttpContext;
				TabulateServerStats (m_Context.Request.HttpMethod);

				psHttpRequestDelegate D;

				if (m_Paths.TryGetValue (m_Context.Request.Url.LocalPath, out D)) 
				{
					D (m_Context);
				} 
				else 
				{
					m_Context.Response.ContentEncoding = System.Text.Encoding.UTF8;
					byte[] Buffer = System.Text.Encoding.UTF8.GetBytes ("404");
					m_Context.Response.ContentLength64 = Buffer.Length;
					m_Context.Response.OutputStream.Write (Buffer, 0, Buffer.Length);
					m_Context.Response.OutputStream.Close ();
					m_Context.Response.Close ();
				}

                return(null);
			} 
			catch (Exception E)
			{
				m_Feedback.Exception(ComponentName, "Error processing request!", E);
                return(null);
			}
		}
		
	}
}
