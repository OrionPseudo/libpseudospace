using System;

namespace Pseudospace.Networking
{
    public interface IpsXMLHttpClient
    {
        TResponse MakeSynchronousRequest<TRequest, TResponse>(string Method, string requestUrl, TRequest obj);
        void MakeAsynchronousRequest<TRequest, TResponse>(string Method, string requestUrl, TRequest obj, psXMLHttpClientCallback<TResponse> Callback);
    }
}

