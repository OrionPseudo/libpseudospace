/*
Copyright Contributors, Pseudospace Virtual Worlds

This file is part of libPseudospace.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Threading;
using System.Collections.Generic;
using Pseudospace.Types;

namespace Pseudospace.ProgramConsole
{
	public class psConsole : IpsConsole, IpsComponent
	{	
		private bool running;
		private Dictionary<string, psConsoleCommand> m_ConsoleCommands;
		private Thread m_ConsoleThread;
		private string m_ConsolePrompt = "Console";
		private IpsFeedbackController m_Feedback;
		private IpsComponentRegistry m_Registry;

        private bool m_Initialized = false;
        private bool m_PostInitalized = false;

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
            m_Registry = ComponentRegistry;
            m_Feedback = m_Registry.GetComponent<IpsFeedbackController>();
            m_ConsoleCommands = new Dictionary<string, psConsoleCommand>(); 

            m_Initialized = true;
        }

        public void PostInitialize()
        {
            Start();
            m_PostInitalized = true;
        }

        public void Refresh()
        {
        	if(m_Initialized) {m_Feedback = m_Registry.GetComponent<IpsFeedbackController>(); }
        }
        
        public string ComponentName { get{return("Program Console");} }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.Middle); } }

        public bool Initialized { get { return(m_Initialized); } }
        public bool PostInitialized { get { return(m_PostInitalized); } }

        public string CommandPrompt { get { return(m_ConsolePrompt); } set { m_ConsolePrompt = value; }}
		
		public void AddHandler(psConsoleCommand Command)
		{
			m_ConsoleCommands.Add(Command.CommandTrigger, Command);
		}
	
		public void RemoveHandler(string CommandText)
		{
			m_ConsoleCommands.Remove(CommandText);
		}
	
		public void Start()
		{
			AddHandler(new psNullConsoleCommand(m_Registry));
			
			running = true;
			m_ConsoleThread = new Thread(new ThreadStart(Listen));
			m_ConsoleThread.Start();
		}
		
		public void Stop()
		{
			running = false;
		}
		
		private void Listen()
		{			
			while(running)
			{
				m_Feedback.ConsoleMessage(m_ConsolePrompt + ": ");
				m_Feedback.ConsoleMessage(ProcessCommand(Console.ReadLine()));
			}
		}
		
		private string ProcessCommand(string command)
		{
			psConsoleCommand m_Command;
			string[] m_CommandText = command.Split(' ');
			string CommandOutput = "";
			
			if(m_ConsoleCommands.ContainsKey(m_CommandText[0]))
			{
				m_Command = m_ConsoleCommands[m_CommandText[0]];
				CommandOutput = m_Command.RunCommand(m_CommandText);
			}
			else if(m_CommandText[0] == "help")
			{
				if(m_CommandText.Length > 1)
				{
					if(m_ConsoleCommands.ContainsKey(m_CommandText[1]))
					{  
						CommandOutput = m_ConsoleCommands[m_CommandText[1]].CommandDetails + " " + m_ConsoleCommands[m_CommandText[1]].CommandHelp + "\n"; 	
					}
					else
					{
						m_Feedback.Bell();
						m_Feedback.Bell();
						CommandOutput = "Help lookup failed, command not found.";
					}
				}
				else
				{
					foreach(KeyValuePair<string, psConsoleCommand> KVP in m_ConsoleCommands)
					{
						CommandOutput += KVP.Key + " " + KVP.Value.CommandDetails + "\n";
					}
				}
			}
			else
			{
			  m_Feedback.Bell();
			  CommandOutput = "Command not found.";
			}

			return(CommandOutput);
		}
	}
	
	public class psNullConsoleCommand : psConsoleCommand
	{
		public psNullConsoleCommand (IpsComponentRegistry _Registry) : base(_Registry)
		{
		}

	    public override string RunCommand(string[] Parameters)
		{
			return("");		
		}
	}
}
