/*
Copyright Contributors, Pseudospace Virtual Worlds

This file is part of libPseudospace.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
using System;

namespace Pseudospace.Types
{
	public interface IpsComponentRegistry
	{
		/// <summary>
   		/// Add an IpsComponent to the registry.</summary>
		bool RegisterComponent<T>(T Component);
		
		/// <summary>
   		/// Get an IpsComponent to the registry.</summary>
		T GetComponent<T>();
		
		/// <summary>
   		/// Check to see if a component belongs to the registry..</summary>
		bool HasComponent<T>();
		
		/// <summary>
   		/// Try to remove an IpsComponent from the registry. (Returns false if component does not exist.)</summary>
		bool TryDeregisterComponent<T>();
	
		/// <summary>
   		/// Add a generic component to the registry.</summary>
        bool RegisterGenericComponent<T>(T Component);
        
       	/// <summary>
   		/// Get a generic component from the registry.</summary>
        T GetGenericComponent<T>();
        
        /// <summary>
   		/// Check to see if a generic component belongs to  to the registry.</summary>
        bool HasGenericComponent<T>();
        
        /// <summary>
   		/// Try to remove a generic component from the registry. (Returns false if component does not exist.)</summary>
        bool TryDeregisterGenericComponent<T>();
        
        /// <summary>
   		/// Force all components (and the registry itself) to refresh their dependant links to other components.</summary>
   		void ForceRefresh();
    }
}

