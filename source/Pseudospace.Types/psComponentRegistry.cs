/*
Copyright Contributors, Pseudospace Virtual Worlds

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the OpenSimulator Project nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE DEVELOPERS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Collections.Concurrent;

namespace Pseudospace.Types
{
	public class psComponentRegistry : IpsComponentRegistry
	{
		private IpsFeedbackController m_Feedback;
		private ConcurrentDictionary<Type, IpsComponent> m_Components = new ConcurrentDictionary<Type, IpsComponent>();
        private ConcurrentDictionary<Type, object> m_GenericComponents = new ConcurrentDictionary<Type, object>();
        private static string LogMarker = "COMPONENT";

        public psComponentRegistry(IpsFeedbackController Feedback)
        {
            m_Feedback = Feedback;
            RegisterComponent<IpsFeedbackController>(Feedback);
        }

        public void Initialize()
        {
        	foreach(psComponentInitializationPriority InitLevel in Enum.GetValues(typeof(psComponentInitializationPriority)))
        	{
	            m_Feedback.Notice(LogMarker, "Setting up " + InitLevel.ToString() + " level components.");
	
	            foreach (IpsComponent C in m_Components.Values)
	            {
	                if(C.InitPriority == InitLevel && !C.Initialized)
	                {
	                    m_Feedback.Notice(LogMarker, "Initializing " + C.ComponentName + ".");
	                    C.Initialize(this);
	                }
	            }
        	}
        }

        public void PostInitialize()
        {
        	foreach(psComponentInitializationPriority InitLevel in Enum.GetValues(typeof(psComponentInitializationPriority)))
        	{
	            m_Feedback.Notice(LogMarker, "Finalizing " + InitLevel.ToString() + " level components.");
	
	            foreach (IpsComponent C in m_Components.Values)
	            {
	                if(C.InitPriority == InitLevel && !C.PostInitialized)
	                {
	                    m_Feedback.Notice(LogMarker, "Post initializing " + C.ComponentName + ".");
	                    C.PostInitialize();
	                }
	            }
        	}
        }
        
        public void ForceRefresh()
        {
        	m_Feedback = GetComponent<IpsFeedbackController>();
        	foreach (IpsComponent C in m_Components.Values)
        	{
        		C.Refresh();
        	}
        }
		
		public bool RegisterComponent<T>(T Component)
        {
            if (Component is IpsComponent)
            {
                IpsComponent ToAdd = (IpsComponent)Component;

                if(ToAdd is IpsReplacableComponent)
                {
                	IpsComponent OldValue;
                	if(m_Components.TryRemove(typeof(T), out OldValue))
                	{
                		IpsReplacableComponent Garbage = (IpsReplacableComponent)OldValue;
                		Garbage.Dispose();
                	}
                	
                	m_Components.TryAdd(typeof(T), (IpsComponent) Component);
                    ForceRefresh();
                    return(true);
                }
                else
                {
                    if (m_Components.TryAdd(typeof(T), ToAdd))
                    {
                    	ForceRefresh();
                        return(true);	
                    } 
                    else
                    {
                        m_Feedback.Error("COMPONENT", "Could not register component" + typeof(T).Name + " - already registered as non replacable.");
                        return(false);	
                    }
                }
            } 
            else
            {
                m_Feedback.Error("COMPONENT", "Attempted to register a non-generic component of the wrong type (" + typeof(T).ToString() + ")!");
                return(false);
            }
		}
		
		public T GetComponent<T>()
		{
			IpsComponent Component;
			
			if(m_Components.TryGetValue(typeof(T), out Component))
			{
				return((T)Component);
			}
			else
			{
				m_Feedback.Error("COMPONENT", typeof(T).ToString() + " was requested but not found in the registry!");
				return(default(T));	
			}
		}
		
		public bool HasComponent<T>()
		{
			return(m_Components.ContainsKey(typeof(T)));
		}

		public bool TryDeregisterComponent<T>()
		{
			IpsComponent Component;
			
			if(m_Components.TryGetValue(typeof(T), out Component))
			{
				if(Component is IpsReplacableComponent)
				{
					IpsComponent Removed;
					
					if(!m_Components.TryRemove(typeof(T), out Removed))
					{
						m_Feedback.Error("COMPONENT", "Unable to remove component (" + typeof(T).ToString() + ").");
					}
					else
					{		
						IpsReplacableComponent Garbage = (IpsReplacableComponent)Removed;
						Garbage.Dispose();
						ForceRefresh();
						return(true);
					}
				}
				else
				{
					m_Feedback.Error("COMPONENT", "Unable to remove component (" + typeof(T).ToString() + "). Component is marked as non-replacable.");
				}
			}
			else
			{
				m_Feedback.Error("COMPONENT", "Requested component to remove (" + typeof(T).ToString() + ") was not found.");
			}
			return(false);
		}
		
        public bool RegisterGenericComponent<T>(T Component)
        {
            m_GenericComponents.AddOrUpdate(typeof(T), Component, (k,v) => v = Component);
            ForceRefresh();
            return(true);
        }
        
        public T GetGenericComponent<T>()
        {
            object Component;
            
            if(m_GenericComponents.TryGetValue(typeof(T), out Component))
            {
                return((T)Component);
            }
            else
            {
                m_Feedback.Error("COMPONENT", "Generic component of type " + typeof(T).ToString() + " was requested but not found in the registry!");
                return(default(T)); 
            }
        }
        
        public bool HasGenericComponent<T>()
        {
            return(m_GenericComponents.ContainsKey(typeof(T)));
        }
        
        public bool TryDeregisterGenericComponent<T>()
		{
			object Component;
			
			if(m_GenericComponents.TryGetValue(typeof(T), out Component))
			{
				if(!m_GenericComponents.TryRemove(typeof(T), out Component))
				{
					m_Feedback.Error("COMPONENT", "Unable to remove generic component (" + typeof(T).ToString() + ").");
				}
				else
				{
					ForceRefresh();
					return(true);
				}
			}
			else
			{
				m_Feedback.Error("COMPONENT", "Requested generic component to remove (" + typeof(T).ToString() + ") was not found.");
			}
			return(false);
		}
	}
}

