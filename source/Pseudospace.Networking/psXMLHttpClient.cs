using System;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

using Pseudospace.Types;

namespace Pseudospace.Networking
{
    public delegate void psXMLHttpClientCallback<T>(T ReturnObject);

    public class psXMLHttpClient : IpsXMLHttpClient, IpsReplacableComponent 
    {
        private interface ICallbackWrapper
        {
            void DoCallback(object ReturnedObject);
            Type ObjectType {get;}
        }

        private class CallbackWrapper<T> : ICallbackWrapper
        {
            private psXMLHttpClientCallback<T> Callback;
            private Type ReturnType;

            public CallbackWrapper(psXMLHttpClientCallback<T> _Callback)
            {
                Callback = _Callback;
                ReturnType = typeof(T);
            }

            public Type ObjectType { get { return(ReturnType); } }
         
            public void DoCallback(object ReturnedObject)
            {
                T ReturningObject = (T)ReturnedObject;
                Callback(ReturningObject);
            }
        }


        private bool m_Initialized = false;
        private IpsComponentRegistry m_Registry;
        private IpsFeedbackController m_Feedback;

        public string ComponentName { get { return("XML Http Client"); } }
        public psComponentInitializationPriority InitPriority { get { return(psComponentInitializationPriority.Middle); } }

        public bool Initialized { get { return(m_Initialized); } }
        public bool PostInitialized { get { return(true); } }

        public void Initialize(IpsComponentRegistry ComponentRegistry)
        {
        	m_Registry = ComponentRegistry;
            m_Feedback = m_Registry.GetComponent<IpsFeedbackController>();
        }

        public void PostInitialize(){}

        public void Refresh()
        {
        	if(m_Initialized) {m_Feedback = m_Registry.GetComponent<IpsFeedbackController>();}
        }
        
        public void Dispose() {}
        
        public TResponse MakeSynchronousRequest<TRequest, TResponse>(string Method, string requestUrl, TRequest obj)
        {
            using (WebClient m_WebClient = new WebClient())
            {
                try
                {
                    m_WebClient.Headers.Add("Content-Type", "text/xml;charset=utf-8");

                    MemoryStream Buffer = new MemoryStream();
                    
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Encoding = System.Text.Encoding.UTF8;

                    XmlSerializer Serializer = new XmlSerializer(typeof(TRequest));
                    XmlWriter XMLOutput = XmlWriter.Create(Buffer, settings);

                    Serializer.Serialize(XMLOutput, obj);

                    byte[] Response = m_WebClient.UploadData(requestUrl, Method, Buffer.ToArray());

                    Serializer = new XmlSerializer(typeof(TResponse));

                    Buffer = new MemoryStream(Response);

                    TResponse ReturnedObj = (TResponse)Serializer.Deserialize(Buffer);
                    return(ReturnedObj);

                }
                catch(Exception E)
                {
                    m_Feedback.Exception(ComponentName, "Error performing synchronous http request!", E);
                    return(default(TResponse));
                }
            }
        }

        public void MakeAsynchronousRequest<TRequest, TResponse>(string Method, string requestUrl, TRequest obj, psXMLHttpClientCallback<TResponse> Callback)
        {
            using (WebClient m_WebClient = new WebClient())
            {
                try
                {
                    m_WebClient.UploadDataCompleted  += HandleUploadDataCompleted;
                    m_WebClient.Headers.Add("Content-Type", "text/xml;charset=utf-8");

                    MemoryStream Buffer = new MemoryStream();
                    
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Encoding = System.Text.Encoding.UTF8;
                  
                    XmlSerializer Serializer = new XmlSerializer(typeof(TRequest));
                    XmlWriter XMLOutput = XmlWriter.Create(Buffer, settings);

                    Serializer.Serialize(XMLOutput, obj);

                    m_WebClient.UploadDataAsync(new Uri(requestUrl), Method, Buffer.ToArray(), new CallbackWrapper<TResponse>(Callback));
                }
                catch(Exception E)
                {
                    m_Feedback.Exception(ComponentName, "Error performing asynchronous http request!", E);
                }
            }
        }

        private void HandleUploadDataCompleted(object sender, UploadDataCompletedEventArgs e)
        {
            try
            {
                ICallbackWrapper m_CallbackWrapper = (ICallbackWrapper)e.UserState;
                MemoryStream Buffer = new MemoryStream(e.Result);
                XmlSerializer Serializer = new XmlSerializer(m_CallbackWrapper.ObjectType);
                m_CallbackWrapper.DoCallback(Serializer.Deserialize(Buffer));
            } 
            catch(Exception E)
            {
                m_Feedback.Exception(ComponentName, "Error handling asynchronous http response!", E);
            }
        }
    }
}

